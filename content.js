const delayInMilliseconds = 100;
let numberOfAttendees = 0;
let lastNumberOfAttendees = 0;
const attendeesList = [];
const attendeesIdByUrl = [];
const attendeesPid = [];
const schoolYears = ['15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'];
const STATUS = { IN: 'IN', OUT: 'OUT' };

let classes = [];
let studentsList = [];

const openAttendeesTab = (event) => {
	let element = event.relatedNode;
	if (element.nodeName === 'DIV') {
		if (element.className === 'TqwH9c') {
			const buttons = document.getElementsByClassName('VfPpkd-Bz112c-LgbsSe yHy1rc eT1oJ JsuyRc boDUxc');
			for (let button of buttons) {
				if (button.ariaLabel === 'Hiển thị tất cả mọi người') {
					button.click();
					setStartTime();
					document.removeEventListener('DOMNodeInserted', openAttendeesTab);
					endMeeting();
				}
			}
		}
	}
};

const getClassesElement = (event) => {
	let element = event.relatedNode;
	if (element.className === 'paged-content-page-container') {
		const classElements = document.getElementsByClassName('aalink coursename');
		if (classElements.length !== 0 && classes.length === 0) {
			getClasses(classElements);
			sendClassesToPopup(classes);
			document.removeEventListener('DOMNodeInserted', getClassesElement);
			setTimeout(() => {
				window.close();
			}, delayInMilliseconds);
		}
	}
};

const startClass = (students) => {
	studentsList = students;
	setStartTime();
	rollCall();
};

const getAttendees = () => {
	setTimeout(function () {
		const newAttendeesListElements = document.querySelectorAll('[role="listitem"]');

		const newAttendeesListUrl = []; //Để kiểm tra có người nào rời không

		for (let attendee of newAttendeesListElements) {
			const [name, url, id] = getNameUrlId(attendee);

			newAttendeesListUrl.push(url);

			if (!attendeesIdByUrl.includes(url)) {
				//Trường hợp có người mới vào
				attendeesIdByUrl.push(url);
				attendeesPid.push(id);
				attendeesList.push(newAttendee(name, url, id));
			} else {
				//Trường hợp người này đã vào trước đây
				const indexOfAttendee = attendeesIdByUrl.indexOf(url);
				if (attendeesList[indexOfAttendee].status === STATUS.OUT) {
					//Trường hợp người này đã ra xong vào lại
					attendeesList[indexOfAttendee] = {
						...attendeesList[indexOfAttendee],
						pid: id,
						timeFlow: [...attendeesList[indexOfAttendee].timeFlow, [STATUS.IN, getTime()]],
						status: STATUS.IN,
					};
					attendeesPid[indexOfAttendee] = id;
				}
			}
		}

		const leaveList = attendeesIdByUrl.filter((url) => newAttendeesListUrl.indexOf(url) == -1);
		for (let leave of leaveList) {
			const indexOfAttendee = attendeesIdByUrl.indexOf(leave);
			if (attendeesList[indexOfAttendee].status === STATUS.IN) {
				attendeesList[indexOfAttendee] = {
					...attendeesList[indexOfAttendee],
					timeFlow: [...attendeesList[indexOfAttendee].timeFlow, [STATUS.OUT, getTime()]],
					status: STATUS.OUT,
				};
			}
		}
	}, delayInMilliseconds);
};

const newAttendee = (name, url, id) => {
	return {
		url: url,
		pid: id,
		name: name,
		timeFlow: [[STATUS.IN, getTime()]],
		status: STATUS.IN,
		attendDuration: null,
	};
};

const isNumberOfAttendeesElement = (element) => {
	if (isNaN(element.newValue)) return false;
	if (element.path[1].className !== 'uGOf1d') return false;
	return true;
};

const calculateTimeAttendClass = () => {
	const startTime = convertTimeToMinutes(sessionStorage.getItem('meeting-start-time'));
	const now = convertTimeToMinutes(getTime());
	const total = now - startTime === 0 ? 1 : now - startTime;
	for (let attendee of attendeesList) {
		let timeAttend = 0;
		for (let i = 0; i < attendee.timeFlow.length; i++) {
			if (attendee.timeFlow[i][0] === STATUS.IN) {
				if (i < attendee.timeFlow.length - 1) {
					if (attendee.timeFlow[i + 1][0] === STATUS.OUT) {
						timeAttend =
							timeAttend +
							convertTimeToMinutes(attendee.timeFlow[i + 1][1]) -
							convertTimeToMinutes(attendee.timeFlow[i][1]);
					}
				} else {
					timeAttend = timeAttend + now - convertTimeToMinutes(attendee.timeFlow[i][1]);
				}
			}
		}
		if (timeAttend === 0) timeAttend = 1;
		attendee.attendDuration = Math.ceil((timeAttend / total) * 100) + '%';
	}
};

const convertTimeToMinutes = (time) => {
	const hour = Number(time.slice(0, 2));
	const minute = Number(time.slice(3, 5));
	return hour * 60 + minute;
};

const setStartTime = () => {
	const meetingStart = getTime();
	console.log('Set start time.');
	sessionStorage.setItem('meeting-start-time', meetingStart);
};

const getTime = () => {
	const now = new Date();
	const time = `${now.getHours() < 10 ? '0' + now.getHours() : now.getHours()}:${
		now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes()
	}`;
	return time;
};

const getNameUrlId = (nodeChild) => {
	//Lấy participant id của google meet
	const id = reduceParticipantId(nodeChild.dataset.participantId);
	//Tách link ảnh
	const firstIndex = nodeChild.innerHTML.indexOf('src=') + 5;
	const lastIndex = nodeChild.innerHTML.slice(firstIndex).indexOf('"');
	const url = nodeChild.innerHTML.slice(firstIndex).slice(0, lastIndex).slice(36);
	//Tách tên
	let name;
	const linebreak = /\n/;
	const lastIndexName = nodeChild.innerText.search(linebreak);
	lastIndexName === -1 ? (name = nodeChild.innerText) : (name = nodeChild.innerText.slice(0, lastIndexName));

	const ifContainYou = name.indexOf('(Bạn)');
	if (ifContainYou !== -1) {
		name = name.slice(0, ifContainYou);
	}

	return [name, url, id];
};

const reduceParticipantId = (participantId) => {
	return participantId.slice(28);
};

const rollCall = () => {
	const messageButtonElement = document.getElementsByClassName('VfPpkd-Bz112c-LgbsSe yHy1rc eT1oJ JsuyRc boDUxc');
	for (let button of messageButtonElement) {
		if (button.ariaLabel === 'Trò chuyện với mọi người') {
			if (button.ariaPressed === 'false') {
				button.click();
			}
		}
	}
	setTimeout(() => {
		const messageAreaElement = document.getElementsByClassName('KHxj8b tL9Q4c')[0];
		const messageSendButton = document.getElementsByClassName('VfPpkd-Bz112c-LgbsSe yHy1rc eT1oJ tWDL4c Cs0vCd')[0];
		if (messageAreaElement && messageSendButton) {
			messageAreaElement.value = 'Điểm danh đột xuất. Sinh viên chat mã số sinh viên để điểm danh trong vòng 2 phút.';
			messageSendButton.disabled = false;
			messageSendButton.click();
			checkRollCall();
		} else {
			rollCall();
		}
	}, delayInMilliseconds * 5);
};

const checkRollCall = () => {
	setTimeout(() => {
		const rollCallOfStudents = document.getElementsByClassName('GDhqjd');
		const rollCallList = [];
		for (let i = rollCallOfStudents.length - 1; i >= 0; i--) {
			const rollCall = rollCallOfStudents[i];
			if (!rollCall.innerText.includes('Điểm danh đột xuất.')) {
				const information = getPidNameStudentid(rollCall);
				// console.log(information);
				if (information.length === 3 && information[0] !== '') {
					rollCallList.push({ pid: information[0], name: information[1], studentId: information[2] });
				}
			} else {
				break;
			}
		}
		mapRollCallToStudentsList(rollCallList);
		mapAttendeesListToStudentsList();
	}, 15000);
};

const mapRollCallToStudentsList = (rollCallList) => {
	for (const rollCall of rollCallList) {
		const indexOfAttendee = attendeesPid.indexOf(rollCall.pid);
		attendeesList[indexOfAttendee] = { ...attendeesList[indexOfAttendee], studentId: rollCall.studentId };
	}
};

const mapAttendeesListToStudentsList = () => {
	for (let index = 0; index < studentsList.length; index++) {
		const attendee = attendeesList.filter((attendee) => attendee.studentId === studentsList[index].studentId)[0];
		if (attendee) {
			studentsList[index] = {
				...studentsList[index],
				attendDuration: attendee.attendDuration,
				status: attendee.status,
				timeFlow: attendee.timeFlow,
			};
		}
	}
};

const checkStudentId = (id) => {
	if (id?.length === 7 && !isNaN(Number(id)) && schoolYears.includes(id?.slice(0, 2))) return true;
	return false;
};

const getPidNameStudentid = (rollCallNode) => {
	if (rollCallNode.lastChild.childElementCount === 1 && checkStudentId(rollCallNode.lastChild.innerText)) {
		return [
			reduceParticipantId(rollCallNode.dataset.senderId),
			rollCallNode.dataset.senderName,
			rollCallNode.lastChild.innerText,
		];
	} else {
		let result = [];
		for (let childNode of rollCallNode.lastChild.childNodes) {
			if (checkStudentId(childNode.dataset.messageText)) {
				result = [
					reduceParticipantId(rollCallNode.dataset.senderId),
					rollCallNode.dataset.senderName,
					childNode.dataset.messageText,
				];
			}
		}
		return result;
	}
};

const endMeeting = () => {
	document
		.getElementsByClassName('VfPpkd-Bz112c-LgbsSe yHy1rc eT1oJ tWDL4c jh0Tpd Gt6sbf QQrMi ftJPW')[0]
		.addEventListener('click', () => {
			setTimeout(() => {
				const end = document.getElementsByClassName('U26fgb O0WRkf oG5Srb C0oVfc kHssdc cz0gRe M9Bg4d');
				end[0].addEventListener('click', () => {
					calculateTimeAttendClass();
					mapAttendeesListToStudentsList();
					console.log(studentsList);
				});
				end[1].addEventListener('click', () => {
					calculateTimeAttendClass();
					mapAttendeesListToStudentsList();
					console.log(studentsList);
				});
			}, delayInMilliseconds);
		});
};

const getClasses = (classElements) => {
	for (let classElement of classElements) {
		const [id, className, studentListUrl] = getIdNameUrlClass(classElement);
		classes.push({ id: id, className: className, studentListUrl: studentListUrl });
	}
};

const getIdNameUrlClass = (classElement) => {
	const classUrl = classElement.href;
	const tempName = classElement.lastChild.data.slice(2);
	const linebreak = /\n/;
	const lastLineBreak = tempName.search(linebreak);
	const className = tempName.slice(0, lastLineBreak).trim();
	const idIndex = classUrl.indexOf('id=');
	const id = classUrl.slice(idIndex + 3);
	const studentListUrl = `http://e-learning.hcmut.edu.vn/user/index.php?id=${id}&perpage=5000`;
	return [id, className, studentListUrl];
};

const getStudentsFromBKEL = (classId, numberOfStudents) => {
	for (let i = 0; i < numberOfStudents; i++) {
		const student = document.getElementById(`user-index-participants-${classId}_r${i}`);
		studentsList.push({ name: student.childNodes[1].innerText, studentId: student.childNodes[2].innerText });
	}
	sendStudentsListToBackground(studentsList);
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.message === 'print') {
		calculateTimeAttendClass();
		mapAttendeesListToStudentsList();
		console.log('Now: ' + getTime());
		console.log('attendeesList: ', attendeesList);
		console.log('studentsList: ', studentsList);
	} else if (request.message === 'roll-call') {
		rollCall();
	} else if (request.message === 'send-students-list') {
		if (request.data) {
			startClass(request.data);
		} else {
			alert('Chưa chọn lớp để lấy danh sách lớp.');
		}
	}
});

const sendClassesToPopup = (classes) => {
	chrome.runtime.sendMessage({
		type: 'classes',
		data: classes,
	});
};

const sendClassesToBackground = (classes) => {
	chrome.runtime.sendMessage({
		type: 'classes',
		data: classes,
	});
};

const sendStudentsListToBackground = (students) => {
	chrome.runtime.sendMessage({
		type: 'students',
		data: students,
	});
};

// Kiểm tra url và chạy các hàm

if (location.host === 'meet.google.com') {
	document.addEventListener('DOMNodeInserted', openAttendeesTab, false);

	document.addEventListener(
		'DOMCharacterDataModified',
		function (event) {
			if (isNumberOfAttendeesElement(event)) {
				getAttendees();
			}
		},
		false
	);
} else if (location.host === 'e-learning.hcmut.edu.vn') {
	if (location.pathname === '/my/') {
		document.addEventListener('DOMNodeInserted', getClassesElement, false);
	} else if (location.pathname === '/user/index.php') {
		const numberOfStudents = document
			.getElementById('participantsform')
			.getElementsByTagName('P')[0]
			.innerText.split(' ')[0];
		const classId = location.search.slice(location.search.indexOf('id=') + 3, location.search.indexOf('perpage') - 1);
		getStudentsFromBKEL(classId, numberOfStudents);
		setTimeout(() => {
			window.close();
		}, delayInMilliseconds);
	}
}
