document.getElementById('objects-edit').onclick = function () {
	const objects = document.getElementById('objects');
	const objectsView = document.getElementById('objects-view').parentNode;
	if (objects.style.display !== 'none') {
		objects.style.display = 'none';
		objectsView.style.display = 'block';
		document.getElementById('objects-edit').textContent = 'Edit';
		document.getElementById('objects-edit').className = '';
		document.getElementById('objects-view').innerHTML = Prism.highlight(
			document.getElementById('objects').value,
			Prism.languages.javascript,
			'javascript'
		);
	} else {
		objects.style.display = 'block';
		objectsView.style.display = 'none';
		document.getElementById('objects-edit').textContent = 'Save';
		document.getElementById('objects-edit').className = 'selected';
	}
};

document.getElementById('every-cell-data-edit').onclick = function () {
	const objects = document.getElementById('every-cell-data');
	const objectsView = document.getElementById('every-cell-data-view').parentNode;
	if (objects.style.display !== 'none') {
		objects.style.display = 'none';
		objectsView.style.display = 'block';
		document.getElementById('every-cell-data-edit').textContent = 'Edit';
		document.getElementById('objects-edit').className = '';
		document.getElementById('every-cell-data-view').innerHTML = Prism.highlight(
			document.getElementById('every-cell-data').value,
			Prism.languages.javascript,
			'javascript'
		);
	} else {
		objects.style.display = 'block';
		objectsView.style.display = 'none';
		document.getElementById('every-cell-data-edit').textContent = 'Save';
		document.getElementById('objects-edit').className = 'selected';
	}
};

document.getElementById('objects').value = JSON.stringify(
	[
		{
			name: 'John Smith',
			dateOfBirth: 999999999999999,
			cost: 1800,
			paid: true,
		},
		{
			name: 'Alice Brown',
			dateOfBirth: 999999999999999,
			cost: 2599.99,
			paid: false,
		},
	],
	null,
	2
).replace(/999999999999999/g, 'new Date()');

document.getElementById('objects-view').innerHTML = document.getElementById('objects').value;

const schema = [
	{
		column: 'Name',
		type: String,
		value: (student) => student.name,
		width: 20,
	},
	{
		column: 'Date of Birth',
		type: Date,
		format: 'mm/dd/yyyy',
		value: (student) => student.dateOfBirth,
		width: 14,
	},
	{
		column: 'Cost',
		type: Number,
		format: '#,##0.00',
		value: (student) => student.cost,
		width: 12,
	},
	{
		column: 'Paid',
		type: Boolean,
		value: (student) => student.paid,
	},
];

const columns = [{ width: 20 }, { width: 14 }, { width: 12 }, {}];

document.getElementById('schema').value = `
[
  {
    column: 'Name',
    type: String,
    value: student => student.name,
    width: 20
  },
  {
    column: 'Date of Birth',
    type: Date,
    format: 'mm/dd/yyyy',
    value: student => student.dateOfBirth,
    width: 14
  },
  {
    column: 'Cost',
    type: Number,
    format: '#,##0.00',
    value: student => student.cost,
    width: 12
  },
  {
    column: 'Paid',
    type: Boolean,
    value: student => student.paid
  }
]
`.trim();

document.getElementById('schema-view').innerHTML = document.getElementById('schema').value;

document.getElementById('every-cell-data').value = JSON.stringify(
	[
		[
			{
				value: 'Name',
				fontWeight: 'bold',
			},
			{
				value: 'Date of Birth',
				fontWeight: 'bold',
			},
			{
				value: 'Cost',
				fontWeight: 'bold',
			},
			{
				value: 'Paid',
				fontWeight: 'bold',
			},
		],
		[
			{
				value: 'John Smith',
				type: 'String',
			},
			{
				value: 999999999999999,
				type: 'Date',
				format: 'mm/dd/yyyy',
			},
			{
				value: 1800,
				type: 'Number',
				format: '#,##0.00',
			},
			{
				value: true,
				type: 'Boolean',
			},
		],
		[
			{
				value: 'Alice Brown',
				type: 'String',
			},
			{
				value: 999999999999999,
				type: 'Date',
				format: 'mm/dd/yyyy',
			},
			{
				value: 2599.99,
				type: 'Number',
				format: '#,##0.00',
			},
			{
				value: false,
				type: 'Boolean',
			},
		],
	],
	null,
	2
)
	.replace(/999999999999999/g, 'new Date()')
	.replace(/"type": "([^",]+)"/g, '"type": $1');

document.getElementById('every-cell-data-view').innerHTML = document.getElementById('every-cell-data').value;

document.getElementById('every-cell-data-columns-view').innerHTML = JSON.stringify(columns, null, 2);

document.getElementById('write-xlsx-file-button-schema').addEventListener('click', () => {
	try {
		const objects = JSON.parse(document.getElementById('objects').value.replace(/new Date\(\)/g, '999999999999999'));
		for (const object of objects) {
			for (const key of Object.keys(object)) {
				if (object[key] === 999999999999999) {
					object[key] = new Date();
				}
			}
		}
		writeXlsxFile(objects, { schema, fileName: 'with-schema.xlsx' });
	} catch (error) {
		console.error(error);
		alert('There was an error. See console output for the error stack trace.');
	}
});

document.getElementById('write-xlsx-file-button-cells').addEventListener('click', () => {
	try {
		const data = JSON.parse(
			document
				.getElementById('every-cell-data')
				.value.replace(/new Date\(\)/g, '999999999999999')
				.replace(/"type": ([^\s,]+)/g, '"type": "$1"')
		);
		for (const row of data) {
			for (const cell of row) {
				if (cell.value === 999999999999999) {
					cell.value = new Date();
				}
				switch (cell.type) {
					case 'Number':
						cell.type = Number;
						break;
					case 'String':
						cell.type = String;
						break;
					case 'Date':
						cell.type = Date;
						break;
					case 'Boolean':
						cell.type = Boolean;
						break;
				}
			}
		}
		writeXlsxFile(data, { columns, fileName: 'cells.xlsx' });
	} catch (error) {
		console.error(error);
		alert('There was an error. See console output for the error stack trace.');
	}
});
