document.getElementById('start-class').onclick = function () {
	chrome.runtime.sendMessage(
		{
			message: 'start-class',
		},
		function (response) {
			console.log(response);
			localStorage.setItem('students', JSON.stringify(response));
			sendStudentsListToContent(response);
		}
	);
};

document.getElementById('print-attendees').onclick = function () {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { message: 'print' });
	});
};

document.getElementById('roll-call').onclick = function () {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { message: 'roll-call' });
	});
};

const sendStudentsListToContent = (studentsList) => {
	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		chrome.tabs.sendMessage(tabs[0].id, { message: 'send-students-list', data: studentsList });
	});
};

const setClasses = () => {
	const classes = JSON.parse(localStorage.getItem('classes'));
	if (classes) {
		document.getElementById('result').innerHTML = classes
			.map(
				(classObject) =>
					`<div class="class-item-container">
					<div>Id: ${classObject.id}</div>
					<a id="class-link" target="_blank" href="${classObject.studentListUrl}" >${classObject.className}</a>
					<hr style="height: 0.5px; border-width: 0; color: #C4C4C4; background-color: #C4C4C4;" >
					</div>`
			)
			.join('');
	} else {
		document.getElementById('result').innerHTML = `<div class="class-item-container">
			<p>Chưa có danh sách lớp học, <a target="blank" href="http://e-learning.hcmut.edu.vn/my/">lấy danh sách lớp học.</a></p>
		</div>`;
	}
};

const getClasses = () => {
	chrome.runtime.sendMessage(
		{
			message: 'get-classes',
		},
		function (response) {
			if (response) {
				localStorage.setItem('classes', JSON.stringify(response));
				setClasses();
			} else {
				setClasses();
			}
		}
	);
};

getClasses();
