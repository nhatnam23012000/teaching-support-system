const style = document.createElement('style');
style.appendChild(
	document.createTextNode(`
  .button { 
    background-color: #ffffff;
		font-size: 16px;
		padding: 12px 25px;
  }
	.button:hover {
		background-color: #eeeeee;
	}
	`)
);

const buttonInject = document.createElement('div');

buttonInject.classList.add('button');
buttonInject.innerHTML = 'Print Attendees List';
buttonInject.appendChild(style);
buttonInject.onclick = () => {
	calculateTimeAttendClass();
	console.log(attendeesList);
};

const buttonRollCalll = document.createElement('div');

buttonRollCalll.classList.add('button');
buttonRollCalll.innerHTML = 'Roll Call';
buttonRollCalll.appendChild(style);
buttonRollCalll.onclick = () => {
	rollCall();
};

const injectHTML = () => {
	const container = document.getElementsByClassName('hWX4r')[0];
	console.log(container);
	container.insertBefore(buttonRollCalll, container.firstChild);
	container.insertBefore(buttonInject, container.firstChild);
};
