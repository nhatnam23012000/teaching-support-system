let classes;
let students;

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
	console.log(request);
	if (request.type === 'students') {
		students = request.data;
	} else if (request.type === 'classes') {
		classes = request.data;
	} else if (request.message === 'start-class') {
		sendResponse(students);
	} else if (request.message === 'get-classes') {
		console.log(classes);
		sendResponse(classes);
	}
});
